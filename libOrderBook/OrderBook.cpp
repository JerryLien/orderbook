#include "OrderBook.h"
#include <cstdio>
#include <stdint.h>
#include <cstdlib>
//------------------------------------------------------------------------------
void OrderBook::SetTableRow(ft_table_t * table, uint32_t level)
{
    uint32_t index = level - 1;
    if (m_Bids.size() >= level)
    {
        if (m_Asks.size() >= level)
            ft_printf_ln(table, "%d|%12d|%12u|%d|%12d|%12u", level, m_Bids[index].GetPrice(), m_Bids[index].GetQty(), level, m_Asks[index].GetPrice(), m_Asks[index].GetQty() );
        else
            ft_printf_ln(table, "%d|%12d|%12u|%d|%12d|%12u", level, m_Bids[index].GetPrice(), m_Bids[index].GetQty(), level, 0, 0);
    }
    else
    {
        if (m_Asks.size() >= level)
            ft_printf_ln(table, "%d|%12d|%12u|%d|%12d|%12u", level, 0,0 , level, m_Asks[index].GetPrice(), m_Asks[index].GetQty() );
        else
            ft_printf_ln(table, "%d|%12d|%12u|%d|%12d|%12u", level, 0,0 , level, 0, 0);
    }
}
//------------------------------------------------------------------------------
void OrderBook::Print()
{
    ft_table_t *table = ft_create_table();
    /* Change border style */
    ft_set_border_style(table, FT_DOUBLE2_STYLE);

    /* Set "header" type for the first row */
    ft_set_cell_prop(table, 0, FT_ANY_COLUMN, FT_CPROP_ROW_TYPE, FT_ROW_HEADER);

    ft_printf_ln(table, "%s|%12s|%12s|%s|%12s|%12s", "Prod ID", "", "", "Channel Seq", "", "Prod Seq");
    ft_printf_ln(table, "%s|%12s|%12s|%12u|%s|%12u", m_ProdID.c_str(), "", "", m_ChannelSeq, "",  m_ProdSeq);
    ft_set_cell_span(table, 0, 0, 3);
    ft_set_cell_span(table, 0, 3, 2);
    ft_set_cell_prop(table, 0, 0, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_CENTER);
    ft_set_cell_span(table, 1, 0, 3);
    ft_set_cell_span(table, 1, 3, 2);
    ft_set_cell_prop(table, 1, 3, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_RIGHT);
    ft_set_cell_prop(table, 1, 5, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_RIGHT);

    ft_set_cell_prop(table, 2, FT_ANY_COLUMN, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_CENTER);
    ft_set_cell_span(table, 2, 0, 3);
    ft_set_cell_span(table, 2, 3, 3);
    ft_write_ln(table, "Buy", "", "", "Sell", "", "");

    for(uint32_t i = 1; i <= 5; i++)
        SetTableRow(table, i);

    ft_set_cell_prop(table, FT_ANY_ROW, 0, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_CENTER);
    ft_set_cell_prop(table, FT_ANY_ROW, 3, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_CENTER);

    ft_set_cell_prop(table, 8, FT_ANY_COLUMN, FT_CPROP_ROW_TYPE, FT_ROW_HEADER);
    ft_printf_ln(table, "%s|%12d|%12u|%s|%12d|%12u", "E", m_ImpliedBid.GetPrice(), m_ImpliedBid.GetQty(), "F", m_ImpliedAsk.GetPrice(), m_ImpliedAsk.GetQty());

    printf("%s\n", ft_to_string(table));
    ft_destroy_table(table);
}
//------------------------------------------------------------------------------
void OrderBook::UpdateMDAction(MDUpdateAction* action)
{
    switch (action->GetAction())
    {
        case MD_UPDATE_ACTION_NEW:
            if(action->GetSide() == MD_SIDE_BUY) {
                m_Bids.insert(m_Bids.begin() + action->m_Level - 1, action->m_PriceNode);
                while(m_Bids.size() > 5)
                    m_Bids.erase(m_Bids.begin() + 5);
            } else {
                m_Asks.insert(m_Asks.begin() + action->m_Level - 1, action->m_PriceNode);
                while(m_Asks.size() > 5)
                    m_Asks.erase(m_Asks.begin() + 5);
            }
            break;
        case MD_UPDATE_ACTION_CHANGE:
            if(action->GetSide() == MD_SIDE_BUY) {
                m_Bids[action->m_Level - 1].SetQty(action->m_PriceNode.GetQty());
            } else {
                m_Asks[action->m_Level - 1].SetQty(action->m_PriceNode.GetQty());
            }
            break;
        case MD_UPDATE_ACTION_DELETE:
            if(action->GetSide() == MD_SIDE_BUY) {
                m_Bids.erase(m_Bids.begin() + action->m_Level - 1);
            } else {
                m_Asks.erase(m_Asks.begin() + action->m_Level - 1);
            }
            break;
        case MD_UPDATE_ACTION_OVERLAY:
            if(action->GetSide() == MD_SIDE_BUY || action->GetSide() == MD_SIDE_SELL) {
                return;
            } else if( action->GetSide() == MD_SIDE_IMPLIED_BUY) {
                m_ImpliedBid.SetPrice(action->m_PriceNode.GetPrice());
                m_ImpliedBid.SetQty(action->m_PriceNode.GetQty());
            } else {
                m_ImpliedAsk.SetPrice(action->m_PriceNode.GetPrice());
                m_ImpliedAsk.SetQty(action->m_PriceNode.GetQty());
            }
            break;
        default: break;
    }
}
//------------------------------------------------------------------------------
void OrderBook::OnI081Update(I081Message* message)
{
    for(size_t i = 0; i < message->ItemCount(); i++)
    {
        MDUpdateAction* pAction = message->GetAction(i);
        UpdateMDAction(pAction);
    }
}
//------------------------------------------------------------------------------
void OrderBook::OnI083Update(I083Message* message)
{
    Clear();
    for(size_t i = 0; i < message->ItemCount(); i++)
    {
        MDUpdateAction* pAction = message->GetAction(i);
        UpdateMDAction(pAction);
    }
}
//------------------------------------------------------------------------------