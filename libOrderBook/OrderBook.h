#ifndef LIBORDERBOOK_ORDERBOOK_H
#define LIBORDERBOOK_ORDERBOOK_H
//------------------------------------------------------------------------------
#include <string>
#include <stdint.h>
#include <vector>
#include "fort.h"
//------------------------------------------------------------------------------
#define MD_UPDATE_ACTION_NEW 0
#define MD_UPDATE_ACTION_CHANGE 1
#define MD_UPDATE_ACTION_DELETE 2
#define MD_UPDATE_ACTION_OVERLAY 5
#define MD_SIDE_BUY '0'
#define MD_SIDE_SELL '1'
#define MD_SIDE_IMPLIED_BUY 'E'
#define MD_SIDE_IMPLIED_SELL 'F'
//------------------------------------------------------------------------------
class PriceNode
{
private:
    int32_t m_Price;
    uint32_t m_Qty;
public:
    PriceNode(int price, uint32_t qty): m_Price(price), m_Qty(qty) {}
    int32_t GetPrice() { return m_Price; }
    uint32_t GetQty() { return m_Qty; }
    void SetPrice(int32_t px) { m_Price = px; }
    void SetQty(uint32_t qty) { m_Qty = qty; }
    void Clear() { m_Price = 0; m_Qty = 0; }
};
//------------------------------------------------------------------------------
class MDUpdateAction
{
private:
    uint8_t m_Action;
    uint8_t m_Side;
    PriceNode m_PriceNode;
    uint8_t m_Level;
public:
    MDUpdateAction(uint8_t action, uint8_t side, int32_t price, uint32_t qty, uint8_t level)
            :m_Action(action), m_Side(side), m_PriceNode(price, qty), m_Level(level) {}

    MDUpdateAction(uint8_t side, int32_t price, uint32_t qty, uint8_t level)
            :m_Action(MD_UPDATE_ACTION_NEW), m_Side(side), m_PriceNode(price, qty), m_Level(level) {}
    uint8_t GetAction() { return m_Action; }
    uint8_t GetSide() { return m_Side; }
    const PriceNode& GetPriceNode(){ return m_PriceNode; }

    friend class OrderBook;
};
//------------------------------------------------------------------------------
class RTMessage
{
private:
    std::vector<MDUpdateAction*> m_Actions;
    uint64_t m_ProdSeq;
    uint64_t m_ChannelSeq;
public:
    size_t ItemCount() { return m_Actions.size(); }
    MDUpdateAction* GetAction(size_t index) { return m_Actions[index]; }
    uint64_t GetProdSeq() { return m_ProdSeq; }
    uint64_t GetChannelSeq() { return m_ChannelSeq; }

    virtual void FromStream(uint8_t* buffer) {}
};
//------------------------------------------------------------------------------
class I081Message : public RTMessage
{
public:
    virtual void FromStream(uint8_t* buffer) {}
};
//------------------------------------------------------------------------------
class I083Message : public RTMessage
{
public:
    virtual void FromStream(uint8_t* buffer) {}
};
//------------------------------------------------------------------------------
class OrderBook
{
private:
    std::string m_ProdID;
    uint32_t m_ChannelSeq;
    uint32_t m_ProdSeq;
    uint8_t m_DecimalLocator;
    std::vector<PriceNode> m_Bids;
    std::vector<PriceNode> m_Asks;
    PriceNode m_ImpliedBid;
    PriceNode m_ImpliedAsk;
public:
    OrderBook(const std::string& prod_id, uint8_t decimal_locator):
        m_ProdID(prod_id), m_ChannelSeq(0), m_ProdSeq(0), m_DecimalLocator(decimal_locator),
        m_ImpliedBid(0,0), m_ImpliedAsk(0,0)
    {
        m_Bids.clear();
        m_Asks.clear();
    }
    ~OrderBook() { m_Bids.clear(); m_Asks.clear();}
    void Print();

    void UpdateChannelSeq(uint32_t seq) { m_ChannelSeq = seq; }
    void UpdateProdSeq(uint32_t seq) { m_ProdSeq = seq; }

    void UpdateMDAction(MDUpdateAction* action);
    void OnI081Update(I081Message* message);
    void OnI083Update(I083Message* message);
    void Clear()
    {
        m_Bids.clear();
        m_Asks.clear();
        m_ImpliedBid.Clear();
        m_ImpliedAsk.Clear();
    }
private:
    void SetTableRow(ft_table_t * table, uint32_t level);
};
//------------------------------------------------------------------------------
#endif //LIBORDERBOOK_ORDERBOOK_H