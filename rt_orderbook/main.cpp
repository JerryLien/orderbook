#include "OrderBook.h"
//------------------------------------------------------------------------------
int main() {
    OrderBook orderBook("TXFI9", 2);

    orderBook.UpdateChannelSeq(1);
    orderBook.UpdateProdSeq(1);
    MDUpdateAction b_action1(MD_UPDATE_ACTION_NEW, MD_SIDE_BUY, 1041200, 42, 1);
    orderBook.UpdateMDAction(&b_action1);

    orderBook.UpdateChannelSeq(3);
    orderBook.UpdateProdSeq(2);
    MDUpdateAction b_action2(MD_UPDATE_ACTION_NEW, MD_SIDE_BUY, 1041100, 74, 2);
    orderBook.UpdateMDAction(&b_action2);

    orderBook.UpdateChannelSeq(6);
    orderBook.UpdateProdSeq(3);
    MDUpdateAction b_action3(MD_UPDATE_ACTION_NEW, MD_SIDE_BUY, 1041000, 67, 3);
    MDUpdateAction b_action4(MD_UPDATE_ACTION_NEW, MD_SIDE_BUY, 1040900, 55, 4);
    MDUpdateAction b_action5(MD_UPDATE_ACTION_NEW, MD_SIDE_BUY, 1040800, 75, 5);
    MDUpdateAction s_action1(MD_UPDATE_ACTION_NEW, MD_SIDE_SELL, 1041300, 20, 1);
    MDUpdateAction s_action2(MD_UPDATE_ACTION_NEW, MD_SIDE_SELL, 1041400, 33, 2);
    MDUpdateAction s_action3(MD_UPDATE_ACTION_NEW, MD_SIDE_SELL, 1041500, 73, 3);
    MDUpdateAction s_action4(MD_UPDATE_ACTION_NEW, MD_SIDE_SELL, 1041600, 31, 4);
    MDUpdateAction s_action5(MD_UPDATE_ACTION_NEW, MD_SIDE_SELL, 1041700, 46, 5);
    orderBook.UpdateMDAction(&b_action3);
    orderBook.UpdateMDAction(&b_action4);
    orderBook.UpdateMDAction(&b_action5);

    orderBook.UpdateMDAction(&s_action1);
    orderBook.UpdateMDAction(&s_action2);
    orderBook.UpdateMDAction(&s_action3);
    orderBook.UpdateMDAction(&s_action4);
    orderBook.UpdateMDAction(&s_action5);
    orderBook.Print();

    orderBook.UpdateChannelSeq(8);
    orderBook.UpdateProdSeq(4);
    MDUpdateAction action_change1(MD_UPDATE_ACTION_CHANGE, MD_SIDE_BUY, 1041000, 30, 3);
    orderBook.UpdateMDAction(&action_change1);
    orderBook.Print();

    orderBook.UpdateChannelSeq(10);
    orderBook.UpdateProdSeq(5);
    MDUpdateAction action_delete1(MD_UPDATE_ACTION_DELETE, MD_SIDE_BUY, 1041000, 0, 3);
    orderBook.UpdateMDAction(&action_delete1);
    orderBook.Print();

    orderBook.UpdateChannelSeq(12);
    orderBook.UpdateProdSeq(6);
    MDUpdateAction action_new(MD_UPDATE_ACTION_NEW, MD_SIDE_BUY, 1041300, 10, 1);
    MDUpdateAction action_delete2(MD_UPDATE_ACTION_DELETE, MD_SIDE_SELL, 1041300, 0, 1);
    MDUpdateAction action_delete3(MD_UPDATE_ACTION_NEW, MD_SIDE_SELL, 1041800, 20, 5);
    orderBook.UpdateMDAction(&action_new);
    orderBook.UpdateMDAction(&action_delete2);
    orderBook.UpdateMDAction(&action_delete3);
    orderBook.Print();

    orderBook.UpdateChannelSeq(13);
    orderBook.UpdateProdSeq(7);
    MDUpdateAction d_action_b(MD_SIDE_IMPLIED_BUY, 1041100, 2, 1);
    MDUpdateAction d_action_s(MD_SIDE_IMPLIED_SELL, 1041500, 2, 1);
    orderBook.UpdateMDAction(&d_action_b);
    orderBook.UpdateMDAction(&d_action_s);
    orderBook.Print();
    return 0;
}
//------------------------------------------------------------------------------