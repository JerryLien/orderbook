cmake_minimum_required(VERSION 2.6)
project(rt_orderbook)

include_directories("../libOrderBook")

set(CMAKE_CXX_STANDARD 98)

add_executable(rt_orderbook main.cpp)
target_link_libraries(rt_orderbook libOrderBook)
